System programming: 8085 Assembler
==================================

Required:

+ gtkmm-3.0
+ pangomm
+ gtksourceviewmm-3.0
+ g++

To compile:
```
#!shell

g++ src/*.cpp src/*/*.cpp `pkg-config pango gtkmm-3.0 gtksourceviewmm-3.0 --cflags --libs`
```

This is the initial road map. Issues and enhancement proposals will
be added to issue tracker in future.
