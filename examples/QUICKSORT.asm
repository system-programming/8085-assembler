START:	LXI SP, 3000H
	    LXI D, 2000H
	    LXI H, 2004H
	    CALL QUICK
	    HLT

QUICK:	MOV A D ; base condition
	    CMP H
	    JC PARTY
	    RNZ
	    MOV A, E
    	CMP L
    	RNC

	
PARTY:	PUSH H
    	PUSH D
	
    	PUSH H
    	MOV A, M
    	MOV C, A
    	MOV H, D
    	MOV L, E
    	DCX D

LOOP:	MOV A, M
    	CMP C
    	JC SWAP
    	JMP CHECK

SWAP:	INX D
    	LDAX D
    	MOV B, A
    	MOV A, M
    	STAX D
    	MOV A, B
    	MOV M, A
	
CHECK:	MOV A, C
    	POP B
    	PUSH PSW
    	MOV A, D
    	CMP B
    	JC NEXT
    	JNZ PIVOT
    	MOV A, E
    	CMP C
    	JNC PIVOT

NEXT:	POP PSW
    	PUSH B
    	MOV C, A
    	JMP LOOP

PIVOT:	POP PSW
        PUSH B
        POP H
    	INX D
    	LDAX D
    	MOV B, A
    	MOV A, M
    	STAX D
    	MOV A, B
    	MOV M, A

    	PUSH D

RECUR:	POP H
    	DCX H
    	POP D
    	CALL QUICK
    	XCHG
    	INX D
    	INX D
    	POP H
    	CALL QUICK
    	RET