lxi sp ffffh
mvi a 10
lxi h 5
call rout
sta 0
hlt

rout:   mov m a
        inx h
        dcr a
        rz
        shld 0
        call rout
        lhld 0
        mov m a
        inx h
        shld 0
        ret
